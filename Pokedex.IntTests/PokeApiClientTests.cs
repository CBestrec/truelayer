using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Pokedex.ApiClients.PokeApi;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Pokedex.ApiClients.PokeApi.Models;
using Moq;

namespace Pokedex.IntTests
{
    [TestClass]
    public class PokeApiClientTests
    {
        private PokeApiClient _pokeApiClient;

        private Mock<IHttpClientFactory> _httpClientFactory;
        private OptionsWrapper<PokeApiClientConfig> _pokeApiClientConfig;

        private const string BaseUrl = @"https://pokeapi.co/api/v2/";

        [TestInitialize]
        public void TestInitialize()
        {
            _httpClientFactory = new Mock<IHttpClientFactory>();
            _httpClientFactory
                .Setup(x => x.CreateClient(It.IsAny<string>()))
                .Returns(new HttpClient());

            _pokeApiClientConfig = new OptionsWrapper<PokeApiClientConfig>(
                new PokeApiClientConfig {BaseUrl = BaseUrl});

            _pokeApiClient = new PokeApiClient(
                _httpClientFactory.Object,
                _pokeApiClientConfig
                );
        }

        [TestMethod]
        public async Task GetPokemonByNameAsync_Given_name_Then_returns_Pokemon()
        {
            //Arrange
            var name = "ditto";

            //Act
            var actual = await _pokeApiClient.GetPokemonByNameAsync(name);

            //Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(name, actual.Name);
            Assert.IsNotNull(actual.Species);
        }

        [TestMethod]
        public async Task GetSpeciesForPokemonAsync_Given_name_Then_returns_Pokemon()
        {
            //Arrange
            var testName = "ditto";
            var pokemon = await _pokeApiClient.GetPokemonByNameAsync(testName);
            Assert.IsNotNull(pokemon);

            //Act
            var actual = await _pokeApiClient.GetSpeciesForPokemonAsync(pokemon);

            //Assert
            Assert.IsNotNull(actual);
            Assert.IsFalse(actual.IsLegendary);
            Assert.IsNotNull(actual.Habitat);
            Assert.AreEqual("urban", actual.Habitat.Name);
            Assert.IsNotNull(actual.FlavorTextEntries);
            Assert.IsTrue(actual.FlavorTextEntries.Any());
            Assert.IsFalse(string.IsNullOrWhiteSpace(actual.FlavorTextEntries.First().FlavorText));
        }

        [TestMethod]
        public async Task GetPokemonAsync_Given_unknown_name_Then_throws_exception()
        {
            //Arrange
            var name = Guid.NewGuid().ToString();

            //Act & Assert
            await Assert.ThrowsExceptionAsync<Exception>(() =>
                _pokeApiClient.GetPokemonByNameAsync(name));
        }

        [TestMethod]
        public async Task GetSpeciesForPokemonAsync_Given_invalid_species_url_Then_throws_exception()
        {
            //Arrange
            var pokemon = new PokemonResponse
            {
                Species = new BaseNamedApiObject {Url = @"https://some.invalid.domain/"}
            };

            //Act & Assert
            await Assert.ThrowsExceptionAsync<HttpRequestException>(() =>
                _pokeApiClient.GetSpeciesForPokemonAsync(pokemon));
        }
    }
}
