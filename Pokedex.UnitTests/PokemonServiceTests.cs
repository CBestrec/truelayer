using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Pokedex.ApiClients.FunTranslations;
using Pokedex.ApiClients.PokeApi;
using Pokedex.ApiClients.PokeApi.Models;
using Pokedex.Services;

namespace Pokedex.UnitTests
{
    [TestClass]
    public class PokemonServiceTests
    {
        private Mock<ILogger<PokemonService>> _loggerMock;
        private PokemonService _pokemonService;

        // Mock API client and fake Pokemons, keyed on name
        private Mock<IPokeApiClient> _pokeApiClientMock;
        private Dictionary<string, PokemonResponse> Pokemons = new Dictionary<string, PokemonResponse>();

        // Mock API client and fake Species, keyed on uri
        private Mock<IFunTranslationsClient> _funTranslationsClientMock;
        private Dictionary<string, SpeciesResponse> Specieses = new Dictionary<string, SpeciesResponse>();


        [TestInitialize]
        public void TestInitialize()
        {
            _loggerMock = new Mock<ILogger<PokemonService>>();
            SetupPokeApiClientMock();
            SetupFunTranslationsClient();
            
            _pokemonService = new PokemonService(
                _loggerMock.Object,
                _pokeApiClientMock.Object,
                _funTranslationsClientMock.Object
                );
        }

        private void SetupFunTranslationsClient()
        {
            _funTranslationsClientMock = new Mock<IFunTranslationsClient>();
            _funTranslationsClientMock
                .Setup(x => x.TranslateAsync(It.IsAny<FunTranslationsLanguage>(), It.IsAny<string>()))
                .ReturnsAsync((FunTranslationsLanguage lang, string text) => $"{lang}__{text}");
        }

        private void SetupPokeApiClientMock()
        {
            _pokeApiClientMock = new Mock<IPokeApiClient>();
            _pokeApiClientMock
                .Setup(x => x.GetPokemonByNameAsync(It.IsAny<string>()))
                .ReturnsAsync((string name) =>
                {
                    Pokemons.TryGetValue(name, out var pokemon);
                    return Utils.DeepClone(pokemon);
                });
            _pokeApiClientMock
                .Setup(x => x.GetSpeciesForPokemonAsync(It.IsAny<PokemonResponse>()))
                .ReturnsAsync((PokemonResponse pr) =>
                {
                    Specieses.TryGetValue(pr?.Species?.Url ?? string.Empty, out var species);
                    return Utils.DeepClone(species);
                });

            //Create 5 fake Species for the mock API client
            const int numSpecies = 5;
            for (var i = 0; i < numSpecies; i++)
            {
                var species = new SpeciesResponse
                {
                    Url = $"species{i}_url",
                    IsLegendary = false,
                    Habitat = new BaseNamedApiObject
                    {
                        Name = $"species{i}_habitat"
                    },
                    FlavorTextEntries = new[]
                    {
                        new FlavorTextItem {FlavorText = $"species{i}_flavor_text1"}
                    }
                };
                Specieses[species.Url] = species;
            }

            //Create 10 fake Pokemon for the mock API client
            const int numPokemons = 10;
            for (var i = 0; i < numPokemons; i++)
            {
                var speciesIndex = i % numSpecies;
                var species = Specieses.ElementAt(speciesIndex).Value;
                var poke = new PokemonResponse { Name = $"poke{i}_name", Species = species};
                Pokemons.Add(poke.Name, poke);
            }
        }

        #region GetByNameAsync()

        [TestMethod]
        public async Task GetByNameAsync_Given_name_Then_returns_Pokemon()
        {
            //Arrange
            var expectedPokemon = Pokemons.ElementAt(0).Value;
            var expectedSpecies = Specieses[expectedPokemon.Species.Url];

            //Act
            var actual = await _pokemonService.GetByNameAsync(expectedPokemon.Name);

            //Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(expectedPokemon.Name, actual.Name);
            Assert.IsFalse(actual.IsLegendary);
            Assert.AreEqual(expectedSpecies.Habitat.Name, actual.Habitat);
            Assert.AreEqual(expectedSpecies.FlavorTextEntries.First().FlavorText, actual.Description);
        }

        [TestMethod]
        public async Task GetByNameAsync_Given_unknown_name_Then_returns_null()
        {
            //Arrange
            var testName = Guid.NewGuid().ToString();

            //Act
            var actual = await _pokemonService.GetByNameAsync(testName);

            //Assert
            Assert.IsNull(actual);
            //Verify exception was logged
            Utils.VerifyLog(_loggerMock, LogLevel.Debug);
        }

        [TestMethod]
        public async Task GetByNameAsync_Given_name_When_pokeapi_client_ex_Then_throws_ex()
        {
            //Arrange
            var testName = Guid.NewGuid().ToString();
            var expectedErrorMessage = $"Failed to get pokemon by name, name={testName}";
            _pokeApiClientMock
                .Setup(x => x.GetPokemonByNameAsync(testName))
                .ThrowsAsync(new Exception("test exception"));

            //Act & Assert
            await Assert.ThrowsExceptionAsync<Exception>(() => 
                _pokemonService.GetByNameAsync(testName));
            //Verify exception was logged
            Utils.VerifyLog(_loggerMock, LogLevel.Error, expectedErrorMessage);
        }
        
        [TestMethod]
        public async Task GetByNameAsync_Given_name_When_species_not_found_Then_returns_null()
        {
            //Arrange
            var testPokemon = Pokemons.ElementAt(0).Value;
            var expectedErrorMessage = $"Species not found, Pokemon.Name={testPokemon.Name}";
            _pokeApiClientMock
                .Setup(x => x.GetSpeciesForPokemonAsync(It.Is<PokemonResponse>(p => p.Name == testPokemon.Name)))
                .ReturnsAsync(default(SpeciesResponse));

            //Act
            var actual = await _pokemonService.GetByNameAsync(testPokemon.Name);

            //Assert
            Assert.IsNull(actual);
            //Verify exception was logged
            Utils.VerifyLog(_loggerMock, LogLevel.Warning, expectedErrorMessage);
        }

        #endregion

        #region GetTranslatedPokemonByNameAsync

        [DataTestMethod]
        [DataRow("cave", false, "yoda")]//cave, not legendary == yoda
        [DataRow("cave", true, "yoda")]//cave, legendary == yoda
        [DataRow("not-cave", true, "yoda")]//not cave, legendary == yoda
        [DataRow("not-cave", false, "shakespeare")]//not cave, not legendary == shakespeare
        public async Task GetTranslatedPokemonByNameAsync_Given_name_Then_returns_translation_Pokemon(
            string habitat, bool isLegendary, string expectedTranslation)
        {
            //Arrange
            var expectedPokemon = Pokemons.ElementAt(2).Value;
            var expectedSpecies = Specieses[expectedPokemon.Species.Url];
            expectedSpecies.Habitat.Name = habitat;
            expectedSpecies.IsLegendary = isLegendary;
            var expectedTranslatedDescription =
                $"{expectedTranslation}__{expectedSpecies.FlavorTextEntries.First().FlavorText}";

            //Act
            var actual = await _pokemonService.GetTranslatedPokemonByNameAsync(expectedPokemon.Name);

            //Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(expectedPokemon.Name, actual.Name);
            Assert.AreEqual(isLegendary, actual.IsLegendary);
            Assert.AreEqual(habitat, actual.Habitat);
            Assert.AreEqual(expectedTranslatedDescription, actual.Description);
        }

        [TestMethod]
        public async Task GetTranslatedPokemonByNameAsync_Given_name_When_translation_api_ex_Then_returns_untranslated()
        {
            //Arrange
            var expectedPokemon = Pokemons.ElementAt(2).Value;
            var expectedSpecies = Specieses[expectedPokemon.Species.Url];
            //Untranslated description
            var expectedTranslatedDescription = expectedSpecies.FlavorTextEntries.First().FlavorText;
            _funTranslationsClientMock
                .Setup(x => x.TranslateAsync(It.IsAny<FunTranslationsLanguage>(), It.IsAny<string>()))
                .ThrowsAsync(new Exception("test exception"));
            var expectedErrorMessage = $"Failed to translate pokemon, name={expectedPokemon?.Name}";

            //Act
            var actual = await _pokemonService.GetTranslatedPokemonByNameAsync(expectedPokemon.Name);

            //Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(expectedPokemon.Name, actual.Name);
            Assert.AreEqual(expectedSpecies.IsLegendary, actual.IsLegendary);
            Assert.AreEqual(expectedSpecies.Habitat.Name, actual.Habitat);
            Assert.AreEqual(expectedTranslatedDescription, actual.Description);
            //Verify exception was logged
            Utils.VerifyLog(_loggerMock, LogLevel.Error, expectedErrorMessage);
        }

        [TestMethod]
        public async Task GetTranslatedPokemonByNameAsync_Given_unknown_name_Then_returns_null()
        {
            //Arrange
            var testName = Guid.NewGuid().ToString();
            var expectedErrorMessage = $"Pokemon not found, name={testName}";

            //Act
            var actual = await _pokemonService.GetTranslatedPokemonByNameAsync(testName);

            //Assert
            Assert.IsNull(actual);
            //Verify 'not found' was logged
            Utils.VerifyLog(_loggerMock, LogLevel.Debug, expectedErrorMessage);
        }

        [TestMethod]
        public async Task GetTranslatedPokemonByNameAsync_Given_name_When_pokemon_api_ex_Then_throws_ex()
        {
            //Arrange
            var testName = Guid.NewGuid().ToString();
            var expectedErrorMessage = $"Failed to get pokemon by name, name={testName}";
            _pokeApiClientMock
                .Setup(x => x.GetPokemonByNameAsync(testName))
                .ThrowsAsync(new Exception("test exception"));

            //Act & Assert
            await Assert.ThrowsExceptionAsync<Exception>(() =>
                _pokemonService.GetTranslatedPokemonByNameAsync(testName));
            //Verify exception was logged
            Utils.VerifyLog(_loggerMock, LogLevel.Error, expectedErrorMessage);
        }

        #endregion
    }
}
