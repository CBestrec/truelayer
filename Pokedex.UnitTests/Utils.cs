﻿using System;
using System.Text.Json;
using Microsoft.Extensions.Logging;
using Moq;

namespace Pokedex.UnitTests
{
    public static class Utils
    {
        public static T DeepClone<T>(T obj)
        {
            var json = JsonSerializer.Serialize(obj);
            var result = JsonSerializer.Deserialize<T>(json);
            return result;
        }

        public static void VerifyLog<T>(Mock<ILogger<T>> loggerMock, LogLevel logLevel, string expectedErrorMessage = null)
        {
            loggerMock.Verify(
                logger => logger.Log(
                    logLevel,
                    It.IsAny<EventId>(),
                    It.Is<It.IsAnyType>((x, _) =>
                        expectedErrorMessage == null || x.ToString().Contains(expectedErrorMessage)),
                    It.IsAny<Exception>(),
                    (Func<It.IsAnyType, Exception, string>)It.IsAny<object>()));
        }
    }
}
