using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Pokedex.Controllers;
using Pokedex.Models;
using Pokedex.Services;

namespace Pokedex.UnitTests
{
    [TestClass]
    public class PokemonControllerTests
    {
        private PokemonController _controller;
        private Mock<ILogger<PokemonController>> _loggerMock;
        private Mock<IPokemonService> _pokemonService;

        private Pokemon _testPokemon;
        private Pokemon _testPokemonTranslated;

        [TestInitialize]
        public void TestInitialize()
        {
            _testPokemon = new Pokemon
            {
                Name = $"name-{Guid.NewGuid()}",
                Description = $"desc-{Guid.NewGuid()}",
                IsLegendary = false,
                Habitat = $"urban-{Guid.NewGuid()}"
            };
            _testPokemonTranslated = Utils.DeepClone(_testPokemon);
            _testPokemonTranslated.Description = $"Trans{_testPokemonTranslated.Description}";
            _testPokemonTranslated.IsLegendary = true;

            _loggerMock = new Mock<ILogger<PokemonController>>();
            _pokemonService = new Mock<IPokemonService>();
            _pokemonService
                .Setup(x => x.GetByNameAsync(It.IsAny<string>()))
                .ReturnsAsync(default(Pokemon));
            _pokemonService
                .Setup(x => x.GetByNameAsync(_testPokemon.Name))
                .ReturnsAsync(_testPokemon);
            _pokemonService
                .Setup(x => x.GetTranslatedPokemonByNameAsync(It.IsAny<string>()))
                .ReturnsAsync(default(Pokemon));
            _pokemonService
                .Setup(x => x.GetTranslatedPokemonByNameAsync(_testPokemonTranslated.Name))
                .ReturnsAsync(_testPokemonTranslated);

            _controller = new PokemonController(
                _loggerMock.Object,
                _pokemonService.Object
                );
        }

        [TestMethod]
        public async Task Get_Given_name_Then_returns_Pokemon()
        {
            //Act
            var actual = await _controller.Get(_testPokemon.Name);

            //Assert
            Assert.IsNotNull(actual);
            Assert.IsInstanceOfType(actual, typeof(ObjectResult));
            var result = (ObjectResult)actual;
            Assert.AreEqual(200, result.StatusCode);
            Assert.IsNotNull(result);

            var resultPokemon = (Pokemon)result.Value;
            Assert.IsNotNull(resultPokemon);
            Assert.AreEqual(_testPokemon.Name, resultPokemon.Name);
            Assert.IsFalse(resultPokemon.IsLegendary);
            Assert.AreEqual(_testPokemon.Habitat, resultPokemon.Habitat);
            Assert.AreEqual(_testPokemon.Description, resultPokemon.Description);
        }
        
        [TestMethod]
        public async Task Get_Given_name_When_error_Then_returns_424()
        {
            //Arrange
            var testName = Guid.NewGuid().ToString();
            var expectedErrorMessage = $"Get failed, name={testName}";
            _pokemonService
                .Setup(x => x.GetByNameAsync(testName))
                .ThrowsAsync(new Exception($"test exception, name={testName}"));

            //Act
            var actual = await _controller.Get(testName);

            //Assert
            Assert.IsNotNull(actual);
            Assert.IsInstanceOfType(actual, typeof(StatusCodeResult));
            var result = (StatusCodeResult)actual;
            Assert.IsNotNull(result);
            Assert.AreEqual((int)HttpStatusCode.FailedDependency, result.StatusCode);
            //Verify exception was logged
            Utils.VerifyLog(_loggerMock, LogLevel.Error, expectedErrorMessage);
        }

        [TestMethod]
        public async Task Get_Given_unknown_name_Then_returns_404()
        {
            //Act
            var actual = await _controller.Get(Guid.NewGuid().ToString());

            //Assert
            Assert.IsNotNull(actual);
            Assert.IsInstanceOfType(actual, typeof(StatusCodeResult));
            var result = (StatusCodeResult)actual;
            Assert.IsNotNull(result);
            Assert.AreEqual((int)HttpStatusCode.NotFound, result.StatusCode);
        }

        [TestMethod]
        public async Task GetTranslated_Given_name_Then_returns_Pokemon_with_expected_translation()
        {
            //Act
            var actual = await _controller.GetTranslated(_testPokemonTranslated.Name);

            //Assert
            Assert.IsNotNull(actual);
            Assert.IsInstanceOfType(actual, typeof(ObjectResult));
            var result = (ObjectResult)actual;
            Assert.AreEqual(200, result.StatusCode);
            Assert.IsNotNull(result);

            var resultPokemon = (Pokemon)result.Value;
            Assert.IsNotNull(resultPokemon);
            Assert.AreEqual(_testPokemonTranslated.Name, resultPokemon.Name);
            Assert.AreEqual(_testPokemonTranslated.IsLegendary, resultPokemon.IsLegendary);
            Assert.AreEqual(_testPokemonTranslated.Habitat, resultPokemon.Habitat);
            Assert.AreEqual(_testPokemonTranslated.Description, resultPokemon.Description);
        }

        [TestMethod]
        public async Task GetTranslated_Given_name_When_exception_Then_returns_424()
        {
            //Arrange
            var testName = Guid.NewGuid().ToString();
            var expectedErrorMessage = $"GetTranslated failed, name={testName}";
            _pokemonService
                .Setup(x => x.GetTranslatedPokemonByNameAsync(testName))
                .ThrowsAsync(new Exception($"test exception, name={testName}"));

            //Act
            var actual = await _controller.GetTranslated(testName);

            //Assert
            Assert.IsNotNull(actual);
            Assert.IsInstanceOfType(actual, typeof(StatusCodeResult));
            var result = (StatusCodeResult)actual;
            Assert.IsNotNull(result);
            Assert.AreEqual((int)HttpStatusCode.FailedDependency, result.StatusCode);
            //Verify exception was logged
            Utils.VerifyLog(_loggerMock, LogLevel.Error, expectedErrorMessage);
        }

        [TestMethod]
        public async Task GetTranslated_Given_unknown_name_Then_returns_404()
        {
            //Act
            var actual = await _controller.GetTranslated(Guid.NewGuid().ToString());

            //Assert
            Assert.IsNotNull(actual);
            Assert.IsInstanceOfType(actual, typeof(StatusCodeResult));
            var result = (StatusCodeResult)actual;
            Assert.IsNotNull(result);
            Assert.AreEqual((int)HttpStatusCode.NotFound, result.StatusCode);
        }
    }
}
