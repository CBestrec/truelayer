﻿using System.Threading.Tasks;
using Pokedex.ApiClients.PokeApi.Models;

namespace Pokedex.ApiClients.PokeApi
{
    public interface IPokeApiClient
    {
        Task<PokemonResponse> GetPokemonByNameAsync(string name);
        Task<SpeciesResponse> GetSpeciesForPokemonAsync(PokemonResponse pokemonResponse);
    }
}
