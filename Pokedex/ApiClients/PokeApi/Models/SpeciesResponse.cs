﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Pokedex.ApiClients.PokeApi.Models
{
    public class SpeciesResponse : BaseNamedApiObject
    {
        [JsonPropertyName("flavor_text_entries")]
        public IEnumerable<FlavorTextItem> FlavorTextEntries { get; set; }

        [JsonPropertyName("is_legendary")]
        public bool IsLegendary { get; set; }

        [JsonPropertyName("habitat")]
        public BaseNamedApiObject Habitat { get; set; }
    }
}
