﻿using System.Text.Json.Serialization;

namespace Pokedex.ApiClients.PokeApi.Models
{
    public class FlavorTextItem
    {
        [JsonPropertyName("flavor_text")]
        public string FlavorText { get; set; }
    }
}
