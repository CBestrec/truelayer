﻿using System.Text.Json.Serialization;

namespace Pokedex.ApiClients.PokeApi.Models
{
    public class PokemonResponse
    {
        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("species")]
        public BaseNamedApiObject Species { get; set; }
    }
}
