﻿using System.Text.Json.Serialization;

namespace Pokedex.ApiClients.PokeApi.Models
{
    public class BaseNamedApiObject
    {
        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("url")]
        public string Url { get; set; }
    }
}
