﻿using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Pokedex.ApiClients.PokeApi.Models;

namespace Pokedex.ApiClients.PokeApi
{
    public class PokeApiClient : BaseApiClient, IPokeApiClient
    {
        public PokeApiClient(
            IHttpClientFactory httpClientFactory,
            IOptions<PokeApiClientConfig> config
            )
            : base(httpClientFactory, config.Value.BaseUrl)
        { }

        public Task<PokemonResponse> GetPokemonByNameAsync(string name)
        {
            var pokemonUrl = $"{BaseUrl}pokemon/{name}";
            return GetEntityAsync<PokemonResponse>(pokemonUrl);
        }

        public Task<SpeciesResponse> GetSpeciesForPokemonAsync(PokemonResponse pokemonResponse)
        {
            return GetEntityAsync<SpeciesResponse>(pokemonResponse.Species.Url);
        }
    }
}
