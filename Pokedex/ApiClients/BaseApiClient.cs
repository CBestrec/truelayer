﻿using System;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace Pokedex.ApiClients
{
    /// <summary>
    /// Base class for API clients
    /// </summary>
    public abstract class BaseApiClient
    {
        protected IHttpClientFactory HttpClientFactory { get; }
        protected string BaseUrl { get; }

        protected BaseApiClient(
            IHttpClientFactory httpClientFactory
            , string baseUrl
            )
        {
            HttpClientFactory = httpClientFactory;
            BaseUrl = baseUrl;
        }

        /// <summary>
        /// Submits the HttpRequest to the httpClient and returns the
        /// result cast to the type specified by <see cref="TResult"/>
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="uri"></param>
        /// <returns></returns>
        protected async Task<TResult> GetEntityAsync<TResult>(string uri)
            where TResult : new()
        {
            // Create a client
            var httpClient = HttpClientFactory.CreateClient();

            // Prepare request
            var httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, uri);

            //Submit request
            var httpResponseMessage = await httpClient.SendAsync(httpRequestMessage);
            if (!httpResponseMessage.IsSuccessStatusCode)
            {
                throw new Exception($"API call failed: code={httpResponseMessage.StatusCode}" +
                                    $", reason={httpResponseMessage.ReasonPhrase}");
            }

            var responseBody = await httpResponseMessage.Content.ReadAsStringAsync();

            var result = JsonSerializer.Deserialize<TResult>(responseBody);

            return result;
        }
    }
}
