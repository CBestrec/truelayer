﻿using System.Threading.Tasks;

namespace Pokedex.ApiClients.FunTranslations
{
    public interface IFunTranslationsClient
    {
        Task<string> TranslateAsync(FunTranslationsLanguage language, string text);
    }
}
