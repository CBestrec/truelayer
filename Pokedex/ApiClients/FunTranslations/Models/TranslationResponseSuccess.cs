﻿using System.Text.Json.Serialization;

namespace Pokedex.ApiClients.FunTranslations.Models
{
    public class TranslationResponseSuccess
    {
        [JsonPropertyName("total")]
        public int Total { get; set; }
    }
}
