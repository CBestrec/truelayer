﻿using System.Text.Json.Serialization;

namespace Pokedex.ApiClients.FunTranslations.Models
{
    public class TranslationResponse
    {
        [JsonPropertyName("success")]
        public TranslationResponseSuccess Success { get; set; }

        [JsonPropertyName("contents")]
        public TranslationResponseContents Contents { get; set; }
    }
}
