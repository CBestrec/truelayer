﻿using System.Text.Json.Serialization;

namespace Pokedex.ApiClients.FunTranslations.Models
{
    public class TranslationResponseContents
    {
        [JsonPropertyName("translated")]
        public string Translated { get; set; }

        [JsonPropertyName("text")]
        public string Text { get; set; }

        [JsonPropertyName("translation")]
        public string Translation { get; set; }
    }
}
