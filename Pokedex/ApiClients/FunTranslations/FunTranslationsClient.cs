﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using Microsoft.Extensions.Options;
using Pokedex.ApiClients.FunTranslations.Models;

namespace Pokedex.ApiClients.FunTranslations
{
    public enum FunTranslationsLanguage
    {
        yoda,
        shakespeare
    };

    public class FunTranslationsClient : BaseApiClient, IFunTranslationsClient
    {
        public FunTranslationsClient(
            IHttpClientFactory httpClientFactory,
            IOptions<FunTranslationsClientConfig> config
            ) : base(httpClientFactory, config.Value.BaseUrl)
        { }

        /// <summary>
        /// Creates the URI for a translation request.
        /// </summary>
        /// <param name="language"></param>
        /// <param name="text"></param>
        /// <returns></returns>
        private string CreateUri(FunTranslationsLanguage language, string text)
        {
            var sanitisedText = SanitiseTextForTranslationRequest(text);
            var uri = $"{BaseUrl}{language}.json?text={sanitisedText}";
            return uri;
        }

        private static string SanitiseTextForTranslationRequest(string text)
        {
            var sanitisedText = text.Replace("\n", " ");
            sanitisedText = HttpUtility.UrlPathEncode(sanitisedText);
            return sanitisedText;
        }

        /// <summary>
        /// Translates the provided text to the specified language.
        /// </summary>
        /// <param name="language"></param>
        /// <param name="text"></param>
        /// <returns></returns>
        public async Task<string> TranslateAsync(FunTranslationsLanguage language, string text)
        {
            var uri = CreateUri(language, text);
            var tr = await GetEntityAsync<TranslationResponse>(uri);
            if (0 == (tr?.Success?.Total ?? 0))
            {
                throw new Exception("Failed to translate text.");
            }

            return tr.Contents.Translated;
        }
    }
}
