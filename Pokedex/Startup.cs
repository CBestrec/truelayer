using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Pokedex.ApiClients.FunTranslations;
using Pokedex.ApiClients.PokeApi;
using Pokedex.Services;

namespace Pokedex
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //Read configuration
            services.Configure<FunTranslationsClientConfig>(
                Configuration.GetSection(nameof(FunTranslationsClientConfig)));
            services.Configure<PokeApiClientConfig>(
                Configuration.GetSection(nameof(PokeApiClientConfig)));

            services.AddControllers();
            
            services.AddHttpClient();//Adds HttpClientFactory in service collection

            //Setup API clients
            services.AddScoped<IPokeApiClient, PokeApiClient>();
            services.AddScoped<IFunTranslationsClient, FunTranslationsClient>();

            //Setup service(s)
            services.AddScoped<IPokemonService, PokemonService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
