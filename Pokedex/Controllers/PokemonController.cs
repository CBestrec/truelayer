﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Net;
using System.Threading.Tasks;
using Pokedex.Services;

namespace Pokedex.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PokemonController : ControllerBase
    {
        private readonly ILogger<PokemonController> _logger;
        private readonly IPokemonService _pokemonService;

        public PokemonController(
            ILogger<PokemonController> logger
            , IPokemonService pokemonService
            )
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _pokemonService = pokemonService ?? throw new ArgumentNullException(nameof(pokemonService));
        }

        [HttpGet]
        [Route("{name}")]
        [Produces("application/json")]
        public async Task<IActionResult> Get([FromRoute] string name)
        {
            try
            {
                var pokemon = await _pokemonService.GetByNameAsync(name);
                if (null == pokemon)
                    return NotFound();
                return Ok(pokemon);
            }
            catch
            {
                _logger.LogError($"Get failed, name={name}");
                IActionResult result = StatusCode((int)HttpStatusCode.FailedDependency);
                return result;
            }
        }

        [HttpGet]
        [Route("translated/{name}")]
        [Produces("application/json")]
        public async Task<IActionResult> GetTranslated([FromRoute] string name)
        {
            try
            {
                var pokemon = await _pokemonService.GetTranslatedPokemonByNameAsync(name);
                if (null == pokemon)
                    return NotFound();
                return Ok(pokemon);
            }
            catch
            {
                _logger.LogError($"GetTranslated failed, name={name}");
                IActionResult result = StatusCode((int)HttpStatusCode.FailedDependency);
                return result;
            }
        }
    }
}
