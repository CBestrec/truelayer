﻿using System.Threading.Tasks;
using Pokedex.Models;

namespace Pokedex.Services
{
    public interface IPokemonService
    {
        Task<Pokemon> GetByNameAsync(string name);
        Task<Pokemon> GetTranslatedPokemonByNameAsync(string name);
    }
}
