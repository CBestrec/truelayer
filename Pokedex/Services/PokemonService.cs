﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Pokedex.ApiClients.FunTranslations;
using Pokedex.ApiClients.PokeApi;
using Pokedex.Models;

namespace Pokedex.Services
{
    public class PokemonService : IPokemonService
    {
        private readonly ILogger<PokemonService> _logger;
        private readonly IPokeApiClient _pokeApiClient;
        private readonly IFunTranslationsClient _funTranslationsClient;
        
        private const string PokemonHabitat_cave = "cave";

        /// <summary>
        /// Provides access to Pokemons
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="pokeApiClient"></param>
        /// <param name="funTranslationsClient"></param>
        public PokemonService(
            ILogger<PokemonService> logger,
            IPokeApiClient pokeApiClient,
            IFunTranslationsClient funTranslationsClient)
        {
            _logger = logger;
            _pokeApiClient = pokeApiClient;
            _funTranslationsClient = funTranslationsClient;
        }

        public async Task<Pokemon> GetByNameAsync(string name)
        {
            try
            {
                //Fetch basic Pokemon data from PokeAPI.
                var pokemonResponse = await _pokeApiClient.GetPokemonByNameAsync(name);
                if (null == pokemonResponse)
                {
                    _logger.LogDebug($"Pokemon not found, name={name}");
                    return null;
                }

                //Fetch Species data from PokeAPI.
                var species = await _pokeApiClient.GetSpeciesForPokemonAsync(pokemonResponse);
                if (null == species)
                {
                    //Warning as the PokeAPI data appears to be inconsistant
                    //(species url from PokemonResponse is not valid)
                    _logger.LogWarning($"Species not found, Pokemon.Name={pokemonResponse.Name}");
                    return null;//Return null as we can't populate the Pokemon
                }

                //Instantiate the result Pokemon.
                var pokemon = new Pokemon
                {
                    Name = pokemonResponse.Name,
                    Description = species.FlavorTextEntries.First().FlavorText,
                    Habitat = species.Habitat.Name,
                    IsLegendary = species.IsLegendary
                };
                return pokemon;
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"Failed to get pokemon by name, name={name}");
                throw;
            }
        }

        #region Translation methods

        public async Task<Pokemon> GetTranslatedPokemonByNameAsync(string name)
        {
            var pokemon = await GetByNameAsync(name);
            if (null == pokemon)//Not found
                return null;

            await TranslatePokemonAsync(pokemon);

            return pokemon;
        }

        private async Task TranslatePokemonAsync(Pokemon pokemon)
        {
            //Translation
            try
            {
                await TranslateDescription(pokemon);
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"Failed to translate pokemon, name={pokemon?.Name}");
                //If the translation has failed we still want to return the pokemon.
            }
        }

        private async Task TranslateDescription(Pokemon pokemon)
        {
            var language = GetTranslationLanguage(pokemon);
            pokemon.Description =
                await _funTranslationsClient.TranslateAsync(language, pokemon.Description);
        }

        private static FunTranslationsLanguage GetTranslationLanguage(Pokemon pokemon)
        {
            var language = (pokemon.IsLegendary || pokemon.Habitat == PokemonHabitat_cave)
                ? FunTranslationsLanguage.yoda
                : FunTranslationsLanguage.shakespeare;
            return language;
        }

        #endregion
    }
}
